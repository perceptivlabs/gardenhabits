//
//  ForestPatch.swift
//  gardenhabits
//
//  Created by Prasenjit Mukherjee on 12/21/19.
//  Copyright © 2019 Prasenjit Mukherjee. All rights reserved.
//

import Foundation
import SceneKit
import ARKit


class ForestPatch: Codable {
  private var patchAnchor: ARAnchor?
  private var patchNode: SCNNode = SCNNode()
  
  private var patchVertices: [vector_float3] = [vector_float3]()
  private var textureCoordinates: [vector_float2] = [vector_float2]()
  private var triangleCount: Int = Int()
  private var triangleIndices: [Int16] = [Int16]()
  private var alignment: ARPlaneAnchor.Alignment = .horizontal
  
  init (anchor: ARPlaneAnchor) {
    self.patchVertices = anchor.geometry.vertices
    self.textureCoordinates = anchor.geometry.textureCoordinates
    self.triangleCount = anchor.geometry.triangleCount
    self.triangleIndices = anchor.geometry.triangleIndices
    self.alignment = .horizontal
    self.patchAnchor = nil
    //add general look and feel of each patch
    setupPatch(inputAnchor: anchor)
  }
  
  init () { //simulation plane
    let simAnchorPose = simd_float4x4()
    self.patchAnchor = ARAnchor(transform: simAnchorPose)
  }
  
  required init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    let transform0 = try container.decode(simd_float4.self, forKey: .transform0)
    let transform1 = try container.decode(simd_float4.self, forKey: .transform1)
    let transform2 = try container.decode(simd_float4.self, forKey: .transform2)
    let transform3 = try container.decode(simd_float4.self, forKey: .transform3)
    let matrix = simd_float4x4(columns: (transform0, transform1, transform2, transform3))
    patchAnchor = ARAnchor(transform: matrix)
    patchVertices = try container.decode([vector_float3].self, forKey: .vertices)
    textureCoordinates = try container.decode([vector_float2].self, forKey: .textureCoordinates)
    triangleIndices = try container.decode([Int16].self, forKey: .triangleIndices)
    triangleCount = try container.decode(Int.self, forKey: .triangleCount)
    
  }
  
  
  func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    try container.encode(patchAnchor?.transform.columns.0, forKey: .transform0)
    try container.encode(patchAnchor?.transform.columns.1, forKey: .transform1)
    try container.encode(patchAnchor?.transform.columns.2, forKey: .transform2)
    try container.encode(patchAnchor?.transform.columns.3, forKey: .transform3)
    try container.encode(patchVertices, forKey: .vertices)
    try container.encode(triangleCount, forKey: .triangleCount)
    try container.encode(textureCoordinates, forKey: .textureCoordinates)
    try container.encode(triangleIndices, forKey: .triangleIndices)
    
  }

  
  enum CodingKeys: String, CodingKey {
    case transform0
    case transform1
    case transform2
    case transform3
//    case alignment //TODO: Add Alignment
    case vertices
    case textureCoordinates
    case triangleCount
    case triangleIndices
  }
  
  func setParentNode(parentNode: SCNNode) {
    print ("parent node set for new anchor")
    parentNode.addChildNode(patchNode)
  }
  
  func getAnchor()->ARAnchor? {
    return patchAnchor
  }

  func setupPatch(inputAnchor: ARPlaneAnchor) {
    
    print("setting up forest patch with inputanchor")
    let vertSrc = SCNGeometrySource(vertices: self.patchVertices.map() {SCNVector3.init($0)})
    let indices: [Int16] = self.triangleIndices
    let inds = SCNGeometryElement(indices: indices, primitiveType: .triangles)
    let geometrySource = SCNGeometry(sources: [vertSrc], elements: [inds])
    self.patchNode = SCNNode(geometry: geometrySource)
    
    let material = self.patchNode.geometry?.materials.first

    if !(material == nil) {
      material!.diffuse.contents = UIColor.green.withAlphaComponent(0.7)
      material!.isDoubleSided = true
    }
    
    self.patchAnchor = ARAnchor(transform: inputAnchor.transform)
  }
  
  func setupPatch() {
    let vertSrc = SCNGeometrySource(vertices: self.patchVertices.map() {SCNVector3.init($0)})
    let indices: [Int16] = self.triangleIndices
    let inds = SCNGeometryElement(indices: indices, primitiveType: .triangles)
    let geometrySource = SCNGeometry(sources: [vertSrc], elements: [inds])
    self.patchNode = SCNNode(geometry: geometrySource)
    
    let material = self.patchNode.geometry?.materials.first

    if !(material == nil) {
      material!.diffuse.contents = UIColor.green.withAlphaComponent(0.7)
      material!.isDoubleSided = true
    }
  }
  
  
  func addTree(model: SCNNode) {
    if self.alignment == .horizontal {
      patchNode.addChildNode(model)
    }
    else {
      //Do Something for vertical planes
    }
  }
  
  func addNode(model: SCNNode) {
    patchNode.addChildNode(model)
  }
  
}
