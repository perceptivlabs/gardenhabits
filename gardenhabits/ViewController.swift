//
//  ViewController.swift
//  gardenhabits
//
//  Created by Prasenjit Mukherjee on 12/17/19.
//  Copyright © 2019 Prasenjit Mukherjee. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import PlacenoteSDK

  
class ViewController: UIViewController, ARSCNViewDelegate {

  @IBOutlet var sceneView: ARSCNView!
  @IBOutlet weak var sceneDebugView: SCNView!
  
  @IBOutlet weak var welcomeView: UIView!
  @IBOutlet weak var sceneCreationView: UIView!
  @IBOutlet weak var finishedView: UIView!
  
  private var player: AVAudioPlayer?

  enum UIState {
    case welcome
    case sceneCreation
    case finished
  };
  private var state: UIState = .welcome
  
  
  private let device = MTLCreateSystemDefaultDevice()!
  
  private var planeAnchorList : [UUID: ARPlaneAnchor] = [:]
  private var planeNodeList : [UUID: SCNNode] = [:]

  private var currForest: Forest!

  
  func getDocumentsDirectory() -> URL {
      let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
      let documentsDirectory = paths[0]
      return documentsDirectory
  }
  
  //MARK: main view functions
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    welcomeView.isHidden = false
    sceneCreationView.isHidden = true
    finishedView.isHidden = true
    
    #if targetEnvironment(simulator)
      let debugScene = SCNScene()
      sceneDebugView.scene = debugScene
      sceneDebugView.allowsCameraControl = true
      sceneDebugView.isHidden = false
      sceneView.isHidden = true
      
    #else
      // Set the view's delegate
      sceneView.delegate = self
      // Show statistics such as fps and timing information
      sceneView.showsStatistics = true
      // Set the scene to the view
      sceneView.scene = SCNScene()
      //sceneView.debugOptions = [SCNDebugOptions.showFeaturePoints]
      sceneView.debugOptions = []
      sceneView.isHidden = false
      sceneDebugView.isHidden = true
    
      currForest = Forest(session: sceneView.session)
    #endif

    
  }
    
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
      
   
    
    // Run the view's session
    #if targetEnvironment(simulator)
      let planeAdded = addPlane(node: sceneDebugView.scene!.rootNode)
      currForest.addPatch(node: planeAdded, horizontal: true)
    #else
      togglePlaneDetection(on: false)
    #endif
    playSound()
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    
    // Pause the view's session
    sceneView.session.pause()
  }
  
  
  // MARK: welcomeView
  @IBAction func newHabit(_ sender: Any) {
    welcomeView.isHidden = true
    sceneCreationView.isHidden = false
    state = .sceneCreation
    togglePlaneDetection(on: true) //start detecting planes

  }
  
  
  @IBAction func plantTree(_ sender: Any) {
    welcomeView.isHidden = true
    finishedView.isHidden = false
    state = .finished //TODO: Add a "localizing" view instead of mis-using the finished view to localize
    player!.play()

    //localize
    
    //load patches
    currForest.loadPatches()
    
    /*for (id,node) in planeNodeList {
      node.removeFromParentNode()

      guard let removeAnchor = planeAnchorList[id] else {
       continue
      }
      sceneView.session.remove(anchor: removeAnchor)
    }
    
    
    planeAnchorList.removeAll()
    planeNodeList.removeAll()

    let configuration = ARWorldTrackingConfiguration()
    configuration.planeDetection = []
    // Run the view's session
    sceneView.session.run(configuration)*/
    
  }
  
  // MARK: sceneCreationView
  @IBAction func finishedCreation(_ sender: Any) {
    sceneCreationView.isHidden = true
    finishedView.isHidden = false
    state = .finished
    #if targetEnvironment(simulator)
        
    #else

      for (id,node) in planeNodeList {
        node.removeFromParentNode()
        
        guard let removeAnchor = planeAnchorList[id] else {
          continue
        }
        
        currForest.addPatch(anchor: removeAnchor)
        sceneView.session.remove(anchor: removeAnchor)
        
      }
      planeAnchorList.removeAll()
      planeNodeList.removeAll()
      currForest.savePatches() //save Forest Patches to hard drive
  
      togglePlaneDetection(on: false)

    #endif
  }
  
  
  // MARK: finishedView
  @IBAction func doneClick(_ sender: Any) {
    finishedView.isHidden = true
    welcomeView.isHidden = false
    
    //TODO: Clear all Scenes
    state = .welcome
    
  }
  
  // MARK: - ARSCNViewDelegate
  
  func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
    switch state {
      case UIState.finished:
        currForest.patchAnchorAdded(patchID: anchor.identifier, parentNode: node)
        break
      case UIState.sceneCreation:
        guard let planeAnchor = anchor as? ARPlaneAnchor else {
          return
        }

        let planeGeometry = ARSCNPlaneGeometry(device: device)!
        planeGeometry.update(from: planeAnchor.geometry)

        let color = planeAnchor.alignment == .horizontal ? UIColor.green : UIColor.systemGreen

        guard let material = planeGeometry.materials.first else { fatalError() }
        material.diffuse.contents = color.withAlphaComponent(0.5)
        let planeNode = SCNNode(geometry: planeGeometry)
        node.addChildNode(planeNode)

        planeAnchorList[anchor.identifier] = planeAnchor
        planeNodeList[anchor.identifier] = planeNode
        break
      case UIState.welcome:
        //Nothing to do. TODO: Save planes w/o rendering them here
        break
    }
  }
  
  func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
    guard let planeAnchor = anchor as? ARPlaneAnchor else {
      return
    }
    
    for childNode in node.childNodes {
      guard let planeGeometry =  childNode.geometry as? ARSCNPlaneGeometry else {
        continue
      }
      planeGeometry.update(from: planeAnchor.geometry)
      planeNodeList[anchor.identifier] = childNode
    }

    planeAnchorList[anchor.identifier] = planeAnchor

  }
  
  func sessionWasInterrupted(_ session: ARSession) {
    // Inform the user that the session has been interrupted, for example, by presenting an overlay
      
  }
  
  func sessionInterruptionEnded(_ session: ARSession) {
    // Reset tracking and/or remove existing anchors if consistent tracking is required
      
  }
  func togglePlaneDetection (on: Bool) {
    let configuration = ARWorldTrackingConfiguration()
    if (on) {
      // Create a session configuration
      configuration.planeDetection = [.horizontal, .vertical]
      sceneView.session.run(configuration)
    }
    else {
      configuration.planeDetection = []
      sceneView.session.run(configuration)
    }
  }
  
  
  func addPlane (node: SCNNode) -> SCNNode {
    
    let width = 2.0
    let height = 2.0
    
    let src = SCNGeometrySource(vertices: [
         SCNVector3(-width / 2, 0, -height / 2),
         SCNVector3(width / 2, 0, -height / 2),
         SCNVector3(-width / 2, 0, height / 2),
         SCNVector3(width / 2, 0, height / 2)
       ])
    
    let indices: [UInt32] = [0, 1, 2, 3]
    let normals = SCNGeometrySource(normals: [SCNVector3](repeating: SCNVector3(0, 0, 1), count: 4))
    
    let inds = SCNGeometryElement(indices: indices, primitiveType: .triangleStrip)
    
    let geometrySource = SCNGeometry(sources: [src, normals], elements: [inds])
    
    let plane = SCNNode(geometry: geometrySource)
    let material = plane.geometry?.materials.first

    if !(material == nil) {
      material!.diffuse.contents = UIColor.red.withAlphaComponent(0.9)
      material!.isDoubleSided = true
    }
    node.addChildNode(plane)
    
    return plane
  }
  
  
  func playSound() {
    let path = Bundle.main.path(forResource: "art.scnassets/lightMusic", ofType:"mp3")!
    let soundUrl = URL(fileURLWithPath: path)
    print ("setup sound")
      do {
        player = try AVAudioPlayer(contentsOf: soundUrl)
        player!.prepareToPlay()
        player!.play()
      } catch {
          // couldn't load file :(
        print ("couldn't play sound")
      }
  }
}
