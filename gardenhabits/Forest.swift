//
//  File.swift
//  gardenhabits
//
//  Created by Prasenjit Mukherjee on 12/21/19.
//  Copyright © 2019 Prasenjit Mukherjee. All rights reserved.
//

import Foundation
import SceneKit
import ARKit
import AVFoundation
import PlacenoteSDK


class Forest {

  
  
  private var forestSession: ARSession?
  private var patches : [UUID: ForestPatch] = [UUID: ForestPatch]()
  //Models to be loaded
  private var treeModel:SCNNode
  private var particleNode: SCNNode
  private var firstPatchAdded:Bool = false
  

 
  init(session: ARSession?) {
    
    print("Loading Forest Assets..")
    guard let treeScene = SCNScene(named: "art.scnassets/tree1.scn") else {
      print ("Can't get Forest Assets (trees). Quitting..")
      fatalError()
    }
    
    treeModel = treeScene.rootNode.childNodes[0]
    self.forestSession = session
    
    guard let particleScene = SCNScene(named: "art.scnassets/particle.scn") else {
      print ("Can't get Forest Assets (trees). Quitting..")
      fatalError()
    }
        
    self.particleNode = particleScene.rootNode.childNodes[0]
    particleNode.position = SCNVector3(0, 0.1, 0.0)

    
  }
  
  func addPatch (anchor: ARPlaneAnchor) {
    if forestSession != nil {
      var patch = ForestPatch(anchor: anchor)
      guard let patchAnchor = patch.getAnchor() else {
        print ("Could not get anchor for ForestPatch. Race Condition?")
        return
      }
      let patchId = patchAnchor.identifier
      patch.addTree(model: treeModel.clone())
      patch.addNode(model: particleNode.clone())
      
      patches[patchId] = patch
      
      print ("anchor added to session")
      forestSession?.add(anchor: patchAnchor)
      
      if (!firstPatchAdded) {
        firstPatchAdded = true
        //playSound()
      }
    }
  }
  
  func patchAnchorAdded (patchID: UUID, parentNode: SCNNode) {
    guard let patch = patches[patchID] else {
      print ("Can't find patch for ID \(patchID)")
      return
    }
    
    print ("anchor node sent back from session")
    patch.setParentNode(parentNode: parentNode)
    
  }
  
  /*func addPatch(node: SCNNode, horizontal: Bool) {//for sim model
    
    var patch = ForestPatch()
    print ("Adding sim model")
    patch.addTree(model: treeModel.clone())
    patches.append(patch)
    if (!firstPatchAdded) {
      firstPatchAdded = true
      playSound()
    }
    
  }*/
  
  
  func getDocumentsDirectory() -> URL {
      let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
      let documentsDirectory = paths[0]
      return documentsDirectory
  }
  
  func savePatches() {
    let fullPath = getDocumentsDirectory().appendingPathComponent("patches")
    
    let mutableData = NSMutableData()
    let archiver = NSKeyedArchiver(forWritingWith: mutableData) //TODO: Update API

    do {
      try archiver.encodeEncodable(Array(patches.values), forKey: NSKeyedArchiveRootObjectKey)
      archiver.finishEncoding()
      try mutableData.write(to: fullPath)
    } catch {
        print("Couldn't write file")
    }
    
  }
    
  func loadPatches() -> Bool {
    let fullPath = getDocumentsDirectory().appendingPathComponent("patches")
    guard let codedData = try? Data(contentsOf: fullPath) else {
      print ("Can't read Data")
      return false
    }
    
    print ("patch data read")
       
    let unarchiver = NSKeyedUnarchiver(forReadingWith: codedData) //TODO: Update API

    do {
      if let loadedPatches = try unarchiver.decodeTopLevelDecodable([ForestPatch].self, forKey: NSKeyedArchiveRootObjectKey) {
        for patch in loadedPatches {
          patch.setupPatch()
          guard let patchAnchor = patch.getAnchor() else {
            print ("Could not get anchor for loaded ForestPatch. Malformed Decoding?")
            return false
          }
          let patchId = patchAnchor.identifier
          patch.addTree(model: treeModel.clone())
          
          var secondModel = treeModel.clone()
          secondModel.position = SCNVector3 (0.1, 0, 0.1)
          secondModel.rotation = SCNQuaternion(x: 0.0, y: 0.247404, z: 0, w: 0.9689124)
          patch.addTree(model: secondModel)
          patch.addNode(model: particleNode.clone())

          
          print ("patches read, adding anchors")

          patches[patchId] = patch
          forestSession?.add(anchor: patchAnchor)
        }
        //playSound()

      }
      else {
        print ("could not unarchive?")

        return true
      }
    } catch {
      print("Couldn't read file.")
      return false
    }
    
    return true
  }
  
  
  

  
  
}
